% Dr. Paolo Ruggeri @UNIL 2019
1 - Make sure that the format selected on pubmed is "Abstract"
2 - Search on Pubmed the wanted keyworkds
3 - Choose "Send to" : 1) File -> Format(Medline). Select "Create File"
4 - Rename the txt file with the wanted name and save it in the folder "download"
5 - open midline_to_excel.m, and select the name of the input TXT (the file saved in point 3) and the output TXT
6-  open in excel and adapt it according to the wanted view