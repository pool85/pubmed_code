% This script takes the midline txt file exported from pubmed and save it into a txt file
% that can be read from excel.
%
% The only parameters to modify are:
% 1) inputTXT -> name of midline txt file exported from pubmed
% 2) outputTXT -> name of the txt file to be processed in excel
% 3) inputfolder -> inputTXT folder path
% 4) outputfolder -> inputTXT folder path
%
% Dr. Paolo Ruggeri @UNIL 2019

%% MODIFY HERE: input and output filenames and folders
inputTXT  = 'insert_inputfilename.txt';
outputTXT = 'insert_outputfilename.txt';
inputfolder = '/your/path/here';
outputfolder = '/your/path/here';




%% get the midline structure and export it into matlab structure. This part is taken from: GETPUBMED Search PubMed database and write results to MATLAB structure %   Copyright 2008-2012 The MathWorks, Inc.
hits = regexp(fileread([inputfolder filesep inputTXT]),'PMID-.*?(?=PMID|</pre>$)','match');

% Instantiate the PMSTRUCT structure that will be returned by GETPUBMED
% to contain six fields.
pmstruct = struct('PubMedID','','PublicationDate','','Title','',...
                 'Abstract','','Authors','','Citation','');

% Loop through each article in hits and extract the PubMed ID,
% publication date, title, abstract, authors, and citation information.
% Place the information in PMSTRUCT, a MATLAB structure array.
for n = 1:numel(hits)
    pmstruct(n).PubMedID         = regexp(hits{n},'(?<=PMID- ).*?(?=\n)','match', 'once');
    pmstruct(n).PublicationDate  = regexp(hits{n},'(?<=DP  - ).*?(?=\n)','match', 'once');
    pmstruct(n).Title            = regexp(hits{n},'(?<=TI  - ).*?(?=PG  -|AB  -)','match', 'once');
    pmstruct(n).Abstract         = regexp(hits{n},'(?<=AB  - ).*?(?=AD  -)','match', 'once');
    pmstruct(n).Authors          = regexp(hits{n},'(?<=AU  - ).*?(?=\n)','match');
    pmstruct(n).Citation         = regexp(hits{n},'(?<=SO  - ).*?(?=\n)','match', 'once');
end


%% save the wanted content into txt file to be imported in excel. 
f = fopen([outputfolder filesep outputTXT], 'w');

for n = 1:length(pmstruct)
    
    Title = strjoin(strsplit(pmstruct(n).Title));
    Abstract = strjoin(strsplit(pmstruct(n).Abstract));
    fprintf(f,'%s %s %s %s %s %s %s\n',pmstruct(n).PubMedID, '$', pmstruct(n).PublicationDate, '$', Title, '$', Abstract);
    
    
end
fclose(f);